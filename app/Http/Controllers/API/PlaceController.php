<?php

namespace App\Http\Controllers\API;

use App\Models\Place;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use MongoDB\BSON\Regex;
use MongoDB\Laravel\Eloquent\Builder;

class PlaceController
{
    private array $selectForLists = [
        '_id',
        'name',
        'categories',
        'thumbnail',
        'short_description',
        'location',
        'rating',
        'facilities',
    ];

    public function index(Request $request)
    {
        return Place::query()
            ->select($this->selectForLists)
            ->when($request->exists('category'), function (Builder $query) use ($request) {
                $query->where('categories', $request->get('category'));
            })
            ->when($request->exists('search'), function (Builder $query) use ($request) {
                $query->where('name', 'REGEX', new Regex($request->get('search'), 'i'));
            })
            ->simplePaginate(10);
    }

    public function show(Place $place): JsonResponse
    {
        return response()->json($place);
    }

    public function explore(Request $request): JsonResponse
    {
        $latitude = $request->query('latitude', '-6.2924303');
        $longitude = $request->query('longitude', '106.725451');
        $maxDistance = $request->query('max_distance', 1000); // in kilometers
        $facilities = $request->query('facilities', []);

        $query = Place::query()
            ->select($this->selectForLists)
            ->when($request->exists('search'), function (Builder $query) use ($request) {
                $query->where('name', 'REGEX', new Regex($request->get('search'), 'i'));
            })
            ->when($request->exists('category'), function (Builder $query) use ($request) {
                $query->where('categories', $request->get('category'));
            })
            ->when($request->exists('min_rating'), function (Builder $query) use ($request) {
                $query->where('rating', '>=', (float) $request->get('min_rating'));
            })
            ->when(count($facilities) > 0, function (Builder $query) use ($request, $facilities) {
                $query->where('facilities', ['$all' => $facilities]);
            });

        $places = $query->get();

        if ($places->isEmpty()) {
            return response()->json([]);
        }

        $scoredPlaces = $this->calculateScores($places, $latitude, $longitude, $maxDistance, $facilities);

        return response()->json($scoredPlaces);
    }

    /**
     * Decision Support System using TOPSIS
     */
    private function calculateScores($places, $latitude, $longitude, $maxDistance, $requiredFacilities): array
    {
        $weights = config('app.weights');
        $distanceArray = [];
        $ratingArray = [];
        $facilityScores = [];
        $filteredPlaces = [];

        foreach ($places as $place) {
            $distance = $this->calculateDistance($latitude, $longitude, $place->location['latitude'], $place->location['longitude']);
            if ($distance <= $maxDistance) {
                $place->distance = $distance;
                $filteredPlaces[] = $place;

                $distanceArray[] = $distance;
                $ratingArray[] = $place->rating;

                // Calculate facility score
                $facilityScore = $this->calculateFacilityScore($place->facilities, $requiredFacilities);
                $facilityScores[] = $facilityScore;
                $place->facility_score = $facilityScore;
            }
        }

        $maxRating = max($ratingArray);
        $maxDistanceScore = max($distanceArray);
        $maxFacilityScore = max($facilityScores);

        $weightedPlaces = [];
        foreach ($filteredPlaces as $place) {
            $distanceScore = $maxDistanceScore - $place->distance; // Closer distances get higher scores
            $ratingScore = $place->rating; // Higher ratings are better
            $facilityScore = $place->facility_score; // Higher facility scores are better

            $totalScore =
                ($weights['rating'] * ($ratingScore / $maxRating)) +
                ($weights['distance'] * ($distanceScore / $maxDistanceScore)) +
                ($weights['facility'] * ($facilityScore / $maxFacilityScore));

            $place->score = $totalScore;
            $weightedPlaces[] = $place;
        }

        usort($weightedPlaces, function ($a, $b) {
            return $b->score <=> $a->score;
        });

        return $weightedPlaces;
    }

    private function calculateDistance($lat1, $lon1, $lat2, $lon2): float|int
    {
        // haversine formula
        $earthRadius = 6371; // Earth radius in kilometers

        $dLat = deg2rad($lat2 - $lat1);
        $dLon = deg2rad($lon2 - $lon1);

        $a = sin($dLat / 2) * sin($dLat / 2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($dLon / 2) * sin($dLon / 2);

        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return $earthRadius * $c;
    }

    private function calculateFacilityScore($placeFacilities, $requiredFacilities): float|int
    {
        if (empty($requiredFacilities)) {
            return count($placeFacilities);
        }

        $matchCount = 0;
        foreach ($requiredFacilities as $facility) {
            if (in_array($facility, $placeFacilities)) {
                $matchCount++;
            }
        }

        return $matchCount;
    }
}
