<?php

namespace App\Http\Controllers\Admin;

use App\Models\Place;
use Illuminate\Contracts\View\View;

class PlaceController
{
    public function index(): View
    {
        $places = Place::paginate(15);
        return view('admin.places.index', compact('places'));
    }

    public function create(): View
    {
        return view('admin.places.form');
    }

    public function edit($id): View
    {
        $place = Place::where('_id', $id)->firstOrFail();
        return view('admin.places.form', compact('place'));
    }
}
