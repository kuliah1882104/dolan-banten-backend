<?php

namespace App\Console\Commands;

use App\Models\Place;
use Illuminate\Console\Command;
use League\Csv\Exception;
use League\Csv\Reader;

class ImportPlace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-place';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle()
    {
        $csv = Reader::createFromPath(storage_path('csv/places.csv'));
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();

        foreach ($records as $offset => $record) {
            $categories = explode(',', $record['category']);
            $facilities = explode(',', $record['facilities']);
            $image_json = json_decode($record['images'], true);

            $images = [];
            foreach ($image_json as $img) {
                $images[] = $img['image'];
            }

            $place = Place::create([
                'name' => $record['title'],
                'categories' => array_values(array_unique($categories)),
                'short_description' => '',
                'descriptions' => $record['descriptions'],
                'location' => [
                    'address' => $record['address'],
                    'latitude' => (float) $record['latitude'],
                    'longitude' => (float) $record['longitude'],
                ],
                'thumbnail' => $record['thumbnail'],
                'images' => $images,
                'price' => $record['price_range'],
                'rating' => (float) $record['review_rating'],
                'facilities' => array_values(array_unique($facilities)),
            ]);

            echo $place->toJson();
        }

        $this->info('Done.');
    }
}
