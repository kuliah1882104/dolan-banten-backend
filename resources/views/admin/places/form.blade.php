<x-layout>
    @if (isset($place))
        <h1>Edit {{ $place->name }}</h1>
    @else
        <h1>Add Place</h1>
    @endif
</x-layout>
