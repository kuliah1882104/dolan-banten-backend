<x-base>
    <div class="drawer lg:drawer-open">
        <input id="my-drawer-2" type="checkbox" class="drawer-toggle"/>
        <div class="drawer-content">
            <div class="w-full flex items-center shadow py-3">
                <label for="my-drawer-2" aria-label="open sidebar" class="btn btn-square btn-ghost">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" class="inline-block w-6 h-6 stroke-current"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                </label>
                <h1 class="font-semibold text-2xl">Dolan Banten</h1>
            </div>
            <!-- Page content here -->
            <main class="p-5">
                {{ $slot }}
            </main>
        </div>
        <div class="drawer-side">
            <label for="my-drawer-2" aria-label="close sidebar" class="drawer-overlay"></label>
            <ul class="menu p-4 w-80 min-h-full bg-base-200 text-base-content">
                <!-- Sidebar content here -->
                <li><a class="{{ request()->routeIs('admin.places*') ? 'active' : '' }}" href="{{ route('admin.places') }}">Tempat Wisata</a></li>
            </ul>
        </div>
    </div>
</x-base>
