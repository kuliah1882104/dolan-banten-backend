<?php

use App\Http\Controllers\API\PlaceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('places', [PlaceController::class, 'explore']);
Route::get('places/dss', [PlaceController::class, 'explore']);
Route::get('places/{place}', [PlaceController::class, 'show']);
