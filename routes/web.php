<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\PlaceController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::middleware('guest')->group(function () {
        Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login');
        Route::post('/login', [AuthController::class, 'authenticate']);
    });

    Route::middleware('auth')->group(function () {
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::view('/', 'admin.index')->name('index');

        Route::get('/places', [PlaceController::class, 'index'])->name('places');
        Route::get('/places/create', [PlaceController::class, 'create'])->name('places.create');
        Route::get('/places/{id}/edit', [PlaceController::class, 'edit'])->name('places.edit');
    });
});
